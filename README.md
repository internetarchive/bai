# hello world

Simplest, fastest way to get a simple webapp up.

Uses small `deno` JS server to answer "hi world",
leveraging the blazing-fast alpine-OS backed [deno] docker container.
